var cv;
var ctx;

window.addEventListener("load", function () {
    $("#line").click(function() {
        draw(Line);
    });
    $("#rect").click(function() {
        draw(Rect);
    });
    $("#oval").click(function() {
        draw(Oval);
    });
    $("#text").click(test);
    cv = $("#dessiner");
    ctx = cv[0].getContext("2d");

});

function test () {
    console.log(this);
}

function draw(drawFunction) {
    cv.unbind("mousedown");
    cv.unbind("mouseup");
    var x1;
    var x2;
    var y1;
    var y2;
    let offset = cv.offset();

    cv.mousedown(function(event) {
        x1 = event.clientX - offset.left;
        y1 = event.clientY - offset.top;
        cv.mouseup(function(event) {
            x2 = event.clientX - offset.left;
            y2 = event.clientY - offset.top;

            drawFunction(x1,y1,x2,y2);
            });
    });
}

function Line(x1,y1,x2,y2) {
    ctx.beginPath();
    ctx.moveTo(x1,y1);
    ctx.lineTo(x2,y2);
    ctx.stroke();

}

function Rect(x1,y1,x2,y2) {
    ctx.rect(x1,y1,x2-x1,y2-y1);
    ctx.stroke(); 
}

/bin/bash: qa: command not found
function Oval(x1, y1, x2, y2) {
    ctx.ellipse(50,50,x2,y2,0,0,2*Math.pi);
    ctx.stroke();
}




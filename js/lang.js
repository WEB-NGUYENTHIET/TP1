window.addEventListener('load',init);

function init()
{
	var fr = document.getElementById("fr");
	var en = document.getElementById("en");
	fr.addEventListener('click',toFrench);
	en.addEventListener('click',toEnglish);
}

function toFrench()
{
	var text = document.querySelectorAll(".lang");
	var i;
	for (i=0;i < text.length;i++)
	{
		if (text[i].classList.contains("fr"))
		{
			text[i].style.display = "initial";
		}
		else
		{
			text[i].style.display = "none";
		}
	}

}

function toEnglish()
{
	var text = document.querySelectorAll(".lang");
	var i;
	for (i=0;i < text.length;i++)
	{
		if (! text[i].classList.contains("fr"))
		{
			text[i].style.display = "initial";
		}
		else
		{
			text[i].style.display = "none";
		}
	}

}


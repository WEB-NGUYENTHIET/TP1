
 <!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>NGUYEN THIET Lam</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript" src="js/lang.js"></script>
<script type="text/javascript" src="js/userColor.js"></script>
  </head>
  <body>
<?php 
session_start();
session_name("lamTP1");
$textUsers = file_get_contents("./data/users.txt");
$textMessages = file_get_contents("./data/messages.txt");

if (isset($_GET["nom"]) && isset($_GET["message"])) {
    $textUsers .= "\n" . $_GET["nom"];
    $textMessages .= "\n" . $_GET["message"];

    file_put_contents("users.txt",$textUsers);
    file_put_contents("messages.txt",$textMessages);
}
$arrayUsers = explode("\n",$textUsers);
$arrayMessages = explode("\n",$textMessages);

array_shift($arrayUsers);
array_shift($arrayMessages);
?>

<div id="left">

 <div id="header">NGUYEN THIET Lam</div>
<article>
	<span class="lang fr">Je joue au tennis:</span>
	<span class="lang en">I play tennis:</span>
<img class="lang fr" alt="Une balle de tennis et une raquette" src="image/tennis.jpg"/>
<img class="lang en" alt="A tennis ball and a racket" src="image/tennis.jpg"/>
	<span class="lang fr" id="legal">Image sous licence <a href="https://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a></span>
	<span class="lang en" id="legal">Image under license <a href="https://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a></span>
</article>


<div id="forum">
<h1 class="lang fr"> Messages des utilisateurs </h1>
<h1 class="lang en"> User messages </h1>


<form action="index.php">
    <label class="lang fr" for="nom">Votre nom : </label><label class="lang en" for="nom">Your name : </label><input required="required" type="text" name="nom"></input>
    <label class="lang fr" for="message">Votre message : </label><label class="lang en" for="message">Your message : </label><input required="required" type="text" name="message"></input>
    <button type="submit" value="send"><span class="lang en">Send</span><span class="lang fr">Envoyer</span></button>
</form>



<div id="message">
<?php 
function printForum($a1,$a2) {
    $max = count($a1);
    for ($i = 0; $i < $max;$i++) {
    echo "<br/><span class=\"forum_pseudo\">" . $a1[$i] . "</span><span class=\"lang fr\"> a dit : </span><span class=\"lang en\"> has said : </span>" . $a2[$i];
    }
}

printForum($arrayUsers,$arrayMessages);


?>
</div>
</div>

<a href="dessiner.html">Dessiner</a>
</div>



<div id="right">
	<div id="language">
<button id="fr">FR 🇫🇷</button>
<button id="en">EN 🇬🇧</button>
</div>
	<p>Info</p>
<div class="info_container">
	<div class="info lang fr">Date de naissance : 19 / 02 / 1997</div>
	<div class="info lang en">Date of birth: 19 / 02 / 1997</div>
	<div class="info lang fr">Lieu de naissance : Villeneuve d'Ascq ( 59 )</div>
	<div class="info lang en">Place of birth: Villeneuve d'Ascq ( 59 )</div>
	<div class="info lang fr">Nationalité : Français </div>
    <div class="info lang en">Nationality : French </div>
<?php 
if (!isset($_SESSION["id"]))
{

echo <<<EOD
    <p>Connectez-vous</p>
<form action="login.php">  
    <input type="text" required="required" name="id"></input>
    <button type="submit" value="send">Connecter</button>
</form>

    <p>Inscrivez-vous</p>
<form action="signup.php">  
    <input type="text" required="required" name="id"></input>
    <button type="submit" value="send">Inscription</button>
</form>
EOD;
} else {
    echo "<p id=\"pseudo\">" . $_SESSION["id"] . "</p>";
    echo <<<EOD
 <p>Déconnectez-vous</p>
<form action="logout.php">  
    <button type="submit" value="send">Déconnexion</button>
</form>


EOD;
}
?>
   
</div>
</div>


</body>
 </html>

upload:
	rm -rf ~/public_html/*
	cp -r ./* ~/public_html/
	chmod -R 757 ~/public_html/data/ids.txt 
	chmod -R 757 ~/public_html/data/users.txt 
	chmod -R 757 ~/public_html/data/messages.txt 
	chmod -R 777 ~/public_html/tmp/
